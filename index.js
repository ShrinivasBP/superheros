const fs = require("fs");
const superheroes = require('superheroes');

const filename = "my-superhero-name";

fs.readFile(filename, "utf-8", (err, data) => {
    if(err) {
        const name = superheroes.random();

        fs.writeFile(filename, name, "utf-8", (err) => {
            if(err) {
                console.error("Error has occurred");
                console.error(err);
            } else {
                console.log("Hello, " + name);
            }
        });
    } else {
        console.log("Hello, " + data);
    }
})